#! -*- coding: utf-8 -*-
from datetime import datetime
from geopy.geocoders import GoogleV3
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from models import Event
from slugify import slugify
import hashlib
import settings

class BorisPipeline(object):
    def __init__(self):
    	engine = create_engine(settings.DB_CONNECTION, echo=True)
        self.Session = sessionmaker(bind=engine)

    def process_item(self, item, spider):
        session = self.Session()

        # Checksum this event against its title and date
        item['checksum'] = hashlib.md5(item['title'] + item['event_date']).hexdigest()
        existing = session.query(Event).filter(Event.checksum == item['checksum']).first()
        
        # Make sure that this event doesn't already exist via its checksum
        if (existing is None):
            # The following fields are not crawler concerns and are universal:

            # Take the event's address and geocode it to aleviate on-the-fly
            # geocoding with the Google Maps JavaScript API. Only do geocodes in
            # the pipeline after the checksum check, to prevent unessecary
            # Geocode API usage/calls.
            geocoder = GoogleV3(api_key=settings.GOOGLE_API_KEY)
            location = geocoder.geocode(item['location'])
            item['latitude'] = location.latitude;
            item['longitude'] = location.longitude;

            item['slug'] = slugify(item['title'])
            item['crawled_at'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

            event = Event(**item)

            # Save the event
            try:
                session.add(event)
                session.commit()
            except:
                session.rollback()
                raise
            finally:
                session.close()

        session.close()
        return item
