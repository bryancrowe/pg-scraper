# -*- coding: utf-8 -*-
import scrapy

class BorisItem(scrapy.Item):
    title = scrapy.Field()
    url = scrapy.Field()
    slug = scrapy.Field()
    event_date = scrapy.Field()
    location = scrapy.Field()
    longitude = scrapy.Field()
    latitude = scrapy.Field()
    source = scrapy.Field()
    crawled_at = scrapy.Field()
    checksum = scrapy.Field()
