# -*- coding: utf-8 -*-
import scrapy
from datetime import datetime
from boris.items import BorisItem

# Joonbug spider
# This spider crawls Joonbugs's NYC nightlife event listings
class JoonbugSpider(scrapy.Spider):
    name = "joonbug"
    allowed_domains = ["joonbug.com"]
    start_urls = (
        'http://joonbug.com/newyork/events',
    )

    def parse(self, response):
        for sel in response.css('div.single-venue-listing'):
            item = BorisItem()

            title = sel.css('a.listing-see-more span.venue-name::text').extract()
            item['title'] = title[0].strip()

            item['url'] = sel.css('a.listing-see-more::attr(href)').extract()
            item['url'] = 'http://joonbug.com' + item['url'][0];

            details = sel.css('div.venue-address::text')
            event_date = details[1].extract().lstrip('  When: ')
            # Format the event's date into a MySQL DateTime format
            item['event_date'] = datetime.strptime(event_date, "%b %d, %Y at %I:%M %p").strftime("%Y-%m-%d %H:%M:%S")

            item['location'] = details[0].extract().lstrip(' Where: ')
            item['source'] = self.name

            yield item
